/// Program that prints a box, an oval, an arrow and a diamond
#include <iostream> /// allows program to perform input and output

/// finction main begins program execution
int
main()
{

    std::cout << "*********         ***             *              *     " << std::endl;
    std::cout << "*       *       *     *          ***            * *    " << std::endl;
    std::cout << "*       *      *       *        *****          *   *   " << std::endl;
    std::cout << "*       *      *       *          *           *     *  " << std::endl;
    std::cout << "*       *      *       *          *          *       * " << std::endl;
    std::cout << "*       *      *       *          *           *     *  " << std::endl;
    std::cout << "*       *      *       *          *            *   *   " << std::endl;
    std::cout << "*       *       *     *           *             * *    " << std::endl;
    std::cout << "*********         ***             *              *     " << std::endl;

    return 0; /// indicaters that program ended successfully
} /// end function main

