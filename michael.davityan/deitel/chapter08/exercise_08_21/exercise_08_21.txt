Question!!!.
---------------------------------------------------------------
// What does this program do?
#include <iostream>
using std::cout;
using std::cin;
using std::endl;

void mystery1( char *, const char * ); // prototype
int 
main() 
{
    char string1[ 80 ];
    char string2[ 80 ];
    cout << "Enter two strings: ";
    cin >> string1 >> string2;
    mystery1( string1, string2 );
    cout << string1 << endl;
    return 0;
}

// What does this function do?
void mystery1
( char *s1, const char *s2 )
{
    while ( *s1 != '\0' ) {
        ++s1;
    }

    for ( ; *s1 = *s2; s1++, s2++ )
    ;
}
--------------------------------------------------------------
--------------------------------------------------------------
Answer!!!.
This program takes from user two strings and merge second to the one.
for example if i insert first string "crazy", and the second "fantomas."
it prints "crazyfantomas."

this function do the merging of two strings
s1 argument of function point to the first element of string1 array,
and s2 argument point to the first element of string2 array.

when user insert two strings from keyboard and program gives that arrays to mystery1 function
while operator in function do preincrement of s1 pointer until its come to array element with value '\0'.
After the end of while s1 pointer point to the element of array with '\0' value.
And so the for operator do a bit copying of string2 array to string1 array 
after the element with value '\0'. That is why the sizes of both of arrays is 80.
The size of string1 must be very large to contain string2.


