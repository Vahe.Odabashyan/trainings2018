#include "headers/core/ChatConnectionListener.hpp"
#include "headers/core/ChatConnection.hpp"
#include "headers/core/ChatServer.hpp"

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <cstring>
#include <iostream>

const int PORT = 8080;

ChatConnectionListener::ChatConnectionListener(ChatServer* server)
    : server_(server)
{
    thread_ = std::thread(&ChatConnectionListener::execute, this);
}

ChatConnectionListener::~ChatConnectionListener()
{
    thread_.join();
}

int
ChatConnectionListener::execute()
{
    mainSocket_ = ::socket(AF_INET, SOCK_STREAM, 0);
    if (mainSocket_ < 0) { 
        std::cerr << "Error 2: Socket creation failed" << std::endl; 
        return 2;
    } 

    /// Forcefully attaching socket to the port 8080
    int option = 1;
    const int optionResult = ::setsockopt(mainSocket_, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &option, sizeof(option));
    if (optionResult != 0) {
        std::cerr << "Error 5: Setting socket options failed" << std::endl; 
        return 1;
    }

    struct sockaddr_in serverAddress;
    ::memset(&serverAddress, '0', sizeof(serverAddress));
    serverAddress.sin_family = AF_INET;
    serverAddress.sin_port = ::htons(PORT);
    serverAddress.sin_addr.s_addr = INADDR_ANY;

    const int bindResult = ::bind(mainSocket_, (struct sockaddr *)&serverAddress, sizeof(serverAddress));
    if (bindResult < 0) {
        std::cerr << "Error 6: Cannot bind to server" << std::endl;
        return 6;
    }

    const int listenResult = ::listen(mainSocket_, 0);
    if (listenResult < 0) {
        std::cerr << "Error 7: Cannot listen to server" << std::endl;
        return 7;
    }

    const int addressLength = sizeof(serverAddress);
    while (true) {
        const int clientSocket = ::accept(mainSocket_, (struct sockaddr *)&serverAddress, (socklen_t*)&addressLength);
        if (clientSocket < 0) {
            std::cerr << "Error 8: Cannot accept connection to server" << std::endl;
            return 8;
        }

        ChatConnection* connection = new ChatConnection(server_, clientSocket);
        server_->addConnection(connection);
    }

    return 0;
}

