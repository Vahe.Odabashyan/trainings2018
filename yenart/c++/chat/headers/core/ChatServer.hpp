#ifndef __CHAT_SERVER__
#define __CHAT_SERVER__

#include <vector>

class ChatConnection;

class ChatServer
{
private:
    typedef std::vector<ChatConnection*> ChatConnections;
public:
    int execute();
    void addConnection(ChatConnection* connection);
    void sendToChatRoom(const char* message, const int messageSize);
public:
    ChatConnections connections_;
};

#endif /// __CHAT_SERVER__

