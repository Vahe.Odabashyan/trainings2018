#include <iostream>

int
main()
{
    for (int firstSide = 3; firstSide <= 500; ++firstSide) {
        for (int secondSide = firstSide + 1; secondSide <= 500; ++secondSide) {
            for (int hypotenuse = secondSide + 1; hypotenuse <= 500; ++hypotenuse) {
                if (hypotenuse < firstSide + secondSide) {
                    if (hypotenuse * hypotenuse == firstSide * firstSide + secondSide * secondSide) {
                        std::cout << "Rectangular triangle sides can be: " 
                                  << hypotenuse << ", " 
                                  << firstSide << ", " 
                                  << secondSide << std::endl;
                        break;
                    }
                }
            }
        }
    }

    return 0;
}
