/// A program that prints the sum, average, product, smallest and largest one of inserted numbers.

#include <iostream> /// allows the program to input and output data

/// function main begins program execution

int
main()
{
    int number1, number2, number3; ///variables where user provided numbers will be stored
    std::cout << "Input three different integers: "; /// prompts the user for data
    std::cin >> number1 >> number2 >> number3; /// read the three numbers

    std::cout << "Sum is " << number1 + number2 + number3 << std::endl;
    std::cout << "Average is " << (number1 + number2 + number3) / 3 << std::endl;
    std::cout << "Product is " << number2 * number2 * number3 << std::endl;
  
    int smallest = number1; /// suppose number1 is the smallest
    if (smallest > number2) {
        smallest = number2; /// update smallest
    }

    if (smallest > number3) {
        smallest = number3; /// update smallest
    }

    std::cout << "Smallest is " << smallest << std::endl;


    int largest = number1; /// suppose number2 is the largest
    if (largest < number2) {
        largest = number2; /// update largest
    }

    if (largest < number3) {
	largest = number3; /// update largest
    }

    std::cout << "Largest is " << largest << std::endl;    

    return 0; /// successful completion of the program
} /// end of function main

