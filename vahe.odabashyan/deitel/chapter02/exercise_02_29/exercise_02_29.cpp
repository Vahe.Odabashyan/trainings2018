/// A program that prints the square and the cube of integers

#include <iostream> /// allows the program to input and output data

/// function main begins program execution

int
main()
{
    std::cout << "integer\tsquare\tcube\n";
    std::cout << 0 << "\t" << 0 * 0 << "\t" << 0 * 0 * 0 << std::endl;
    std::cout << 1 << "\t" << 1 * 1 << "\t" << 1 * 1 * 1 << std::endl;
    std::cout << 2 << "\t" << 2 * 2 << "\t" << 2 * 2 * 2 << std::endl;
    std::cout << 3 << "\t" << 3 * 3 << "\t" << 3 * 3 * 3 << std::endl;
    std::cout << 4 << "\t" << 4 * 4 << "\t" << 4 * 4 * 4 << std::endl;
    std::cout << 5 << "\t" << 5 * 5 << "\t" << 5 * 5 * 5 << std::endl;
    std::cout << 6 << "\t" << 6 * 6 << "\t" << 6 * 6 * 6 << std::endl;
    std::cout << 7 << "\t" << 7 * 7 << "\t" << 7 * 7 * 7 << std::endl;
    std::cout << 8 << "\t" << 8 * 8 << "\t" << 8 * 8 * 8 << std::endl;
    std::cout << 9 << "\t" << 9 * 9 << "\t" << 9 * 9 * 9 << std::endl;
    std::cout << 10 << "\t" << 10 * 10 << "\t" << 10 * 10 * 10 << std::endl;   

    return 0; /// successful completion of the program
} /// end function main

