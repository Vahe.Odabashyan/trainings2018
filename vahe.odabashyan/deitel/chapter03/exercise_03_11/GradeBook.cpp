#include "GradeBook.hpp"
#include <iostream>

GradeBook::GradeBook(std::string name, std::string instructor)
{
    setCourseName(name);
    setInstructorName(instructor);
}

void
GradeBook::setCourseName(std::string name)
{
    courseName_ = name;
}

std::string
GradeBook::getCourseName()
{
    return courseName_;
}

void
GradeBook::setInstructorName(std::string instructor)
{
    instructorName_ = instructor;
}

std::string
GradeBook::getInstructorName()
{
    return instructorName_;
}

void
GradeBook::displayMessage()
{
    std::cout << "Welcome to the grade book for\n" << getCourseName() << "!" << "\n" << "This course is presented by: " << getInstructorName() << std::endl;
}

