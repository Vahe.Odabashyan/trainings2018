#include "Date.hpp"
#include <iostream>

Date::Date(int month, int day, int year)
{
    setMonth(month);
    setDay(day);
    setYear(year);
}

void
Date::setMonth(int month)
{
    if(month < 1 || month > 12) {
        month_ = 1;
	std::cout << "Warning 1: Month value must be in the range 1-12. Month value has been set to 1!\n";
	return;
    }    
    month_ = month;
}

int
Date::getMonth()
{
    return month_;
}

void
Date::setDay(int day)
{
    if(day < 1 || day > 31) {
        day_ = 1;
        std::cout << "Warning 2: Day value must be in the range 1-31. Day value has been set to 1!\n";
        return;	
    }
    day_ = day;
}

int
Date::getDay()
{
    return day_;
}

void
Date::setYear(int year)
{
    if(year <= 0) {
        year = 2019;
	std::cout << "Warning 3: Year value cannot be smaller than or equal to 0. Year value is set to 2019!\n";
	return;
    }
    year_ = year;
}

int
Date::getYear()
{
    return year_;
}

void
Date::displayDate()
{
    std::cout << getMonth() << "/" << getDay() << "/" << getYear() << std::endl;
}

