#include "GradeBook.hpp"
#include <iostream>

GradeBook::GradeBook(std::string course, std::string instructor)
{
    setCourseName(course);
    setInstructorName(instructor);
}

void
GradeBook::setCourseName(std::string name)
{
    courseName_ = name;
}

std::string
GradeBook::getCourseName()
{
    return courseName_;
}

void
GradeBook::setInstructorName(std::string name)
{
    instructorName_ = name;
}

std::string
GradeBook::getInstructorName()
{
    return instructorName_;
}

void
GradeBook::displayMessage()
{
    std::cout << "Welcome to grade book for\n" << getCourseName() << "!" << std::endl;
    std::cout << "This course is presented by: " << getInstructorName() << std::endl;
}

