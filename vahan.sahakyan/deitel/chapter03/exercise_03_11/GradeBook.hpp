#include <string>

class GradeBook
{
public:
    GradeBook(std::string course, std::string instructor);
    void setCourseName(std::string course);
    std::string getCourseName();
    void setInstructorName(std::string instructor);
    std::string getInstructorName();
    void displayMessage();
private:
    std::string courseName_;
    std::string instructorName_;
};

