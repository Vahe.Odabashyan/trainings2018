#include <iostream>

int
main()
{
    std::cout << "N\t" << "10*N\t" << "100*N\t" << "1000*N\n" << std::endl;
    int number = 1;
    while (number <= 5) {
        std::cout << number << '\t' << 10 * number << '\t' << 100 * number << '\t' << 1000 * number << std::endl;
        ++number;
    }
    return 0;
}

