#include <iostream>

int
main()
{
    int y; /// declare y
    int x = 1; /// initialize x
    int total = 0; /// initialize total

    while ( x <= 10 ) { /// loop 10 times
        y = x * x; /// perform calculation
        std::cout << y << std::endl; /// output result
        total += y; /// add y to total
        x++; /// increment counter x
    } /// end while

    std::cout << "Total is " << total << std::endl; /// display result
    return 0; /// indicate successful termination
} /// end main

/// this program prints a sequence of numbers and the total value in the end
/// 1
/// 4
/// 9
/// 16
/// 25
/// 36
/// 49
/// 64
/// 81
/// 100
/// Total is 385

