<?php

namespace Models;
use Libs\Model;

///////////////////////////////////////////////////////////////////
// 
// class Logo extends Model
//
// Work with Logo table in database
//
// public function __construct()
// public function get($id = NULL)
// 
// private $logoes_ = [];
//
///////////////////////////////////////////////////////////////////

class Logo extends Model
{
    /**
     * The site logoes array
     */
    private $logoes_ = [];

    /**
     * The constructor calls parent constructor
     * and gets the db connection
     */
    function __construct()
    {
        // TODO: Save it in db
        $this->logoes_ = [
            "assets/images/background/INSTITUTE-C-logo.png",
            "assets/images/background/cpp.png"
        ];
        parent:: __construct();
    }

    /**
     * Returns the logoes from logoes table in db
     *
     * @param int $id - id of currrent logo in db
     * @return array $this->_logoes - returns the loges array
     *         | current logo which id = $id | NULL
     */
    public function get($id = NULL)
    {
        if (is_null($id)) {
            return $this->logoes_;
        }
        return NULL;
    }
}
?>
