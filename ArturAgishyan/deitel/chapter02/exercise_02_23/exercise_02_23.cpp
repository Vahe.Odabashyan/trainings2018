#include <iostream>

int 
main() 
{ 
    int number1, number2, number3, number4, number5;
    std::cout << "Type 5 numbers:";
    std::cin >> number1 >> number2 >> number3 >> number4 >> number5;

    int small = number1;
    if (number2 < small) {
         small = number2;
    }
    if (number3 < small) {
         small = number3;
    }
    if (number4 < small) {
         small = number4;
    }
    if (number5 < small) {
         small = number5;
    }
    std::cout << "Smallest number:" << small << std::endl;
                                                                          
    int large = number1;
    if (number2 > large) {
         large = number2;
    }
    if (number3 > large) {
         large = number3;
    }
    if (number4 > large) {
         large = number4;
    }
    if (number5 > large) {
         large = number5;
    }
    std::cout << "Largest number:" << large << std::endl;

    return 0;
}

